import React, {useState} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function App() {
  const [outputText, setOutputText] = useState('Assalamulaikum..')
  return (
    <View style={styles.container}>
      <Text>{outputText}</Text>
      
      <Button title="Change" onPress={() => setOutputText('Selamat datang')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
